
import { useSelector, useDispatch } from 'react-redux'
import { decrement, increment } from './reducers/counter.js'
export default function App(){
	const count = useSelector((state) => state.counter.value)
  const dispatch = useDispatch()

  return(
    <>
    home App
    <button
          aria-label="Increment value"
          onClick={() => dispatch(increment())}
        >
          Increment
        </button>
        <span>{count}</span>
        <button
          aria-label="Decrement value"
          onClick={() => dispatch(decrement())}
        >
          Decrement
        </button>

        <input type="textbox"/>
        </>)
}